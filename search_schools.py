
import csv
import time

class schoolSearch:

	def __init__(self, csvFile='school_data.csv',delimiter=','):
		self.csvFile   = csvFile
		self.delimiter = delimiter
		try:
			self.school=self.read_csv_2_dictobj()
		except:
			print('Error unable to execute self.read_csv_2_dictobj')

	def read_csv_2_dictobj(self):
		'''
		This Method reads the csv file and retuns the list  which of dictionaries (one dict for record)
		'''
		print('File to Open:',self.csvFile)
		try:
			reader = csv.DictReader(open(self.csvFile), delimiter = self.delimiter)
			print('After reading the file successfully')
		except:
			print('Error Opening the file to read:',self.csvFile)
		result = []
		for row in reader:
			rowdictobj = {}
			for column,value in row.items():
				rowdictobj[column] = value
			result.append(rowdictobj)
		return result

	def search_school(self,searchwords):
		st_time  = time.time()
		resultcol = []
		tmplist   = []
		sw        = searchwords.split()
		for col in self.school:
			if len(col['SCHNAM05']) != 0: # Ignore the records with no school
				tmplist.clear()
				tmplist.append(col['LSTATE05'])
				tmplist.append(col['LCITY05'])
				tmplist.append(col['SCHNAM05'])
				resutcol = resultcol.append(tmplist.copy())
		matching    = []
		for s in resultcol:
			ctr_city    = 0
			ctr_state   = 0
			ctr_school  = 0
			for fw in sw:
				'''
				If we do not have to search the words in city and state then deactivate the below
				two if statements, also if we add them as separate values
				we can make more complex listing based on top finds in school,city, state and
				the combinations. Combined freqency ranking is done for now.
				'''
				# for some reason the upper used directly with if didn't work have to try later.
				fw = fw.upper()
				s0 = s[0].upper()
				s1 = s[1].upper()
				s2 = s[2].upper()
				if fw in s0:
					ctr_state  = ctr_state  + s[0].count(fw)
					#print(' FndWord:' + fw + ' InStr:' + s[2] + ' Count Freq:' + str(s[2].count(fw)))
				if fw in s1:
					ctr_city   = ctr_city   + s[1].count(fw)
					#print(' FndWord:' + fw + ' InStr:' + s[2] + ' Count Freq:' + str(s[2].count(fw)))
				if fw in s2:
					ctr_school = ctr_school + s[2].count(fw)
					#print(' FndWord:' + fw + ' InStr:' + s[2] + ' Count Freq:' + str(s[2].count(fw)))
			wordfreq = ctr_state + ctr_city + ctr_school
			if wordfreq > 0:
				s.append(wordfreq)
				matching.append(s)
		matching = sorted(matching, key = lambda x: x[3], reverse = True)
		print('Top 3 schools matching for the search words:',searchwords)
		ctr = 1
		for i in matching:
			if ctr > 3:
				break
			print(str(ctr) + '. School:' + i[2])
			print('City, State:' + i[1] + ', ' + i[0])
			ctr += 1
			#print('-----DEBUG------',i)
		end_time = time.time()
		print('Total execution time:', end_time - st_time)