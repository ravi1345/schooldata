import csv
class schoolCount:

	def __init__(self, csvFile='school_data.csv',delimiter=','):
		self.csvFile   = csvFile
		self.delimiter = delimiter
		self.result    = self.read_csv_2_dictobj();
		#print('csvfile:',self.csvFile)
	def read_csv_2_dictobj(self):
		'''
		This Method reads the csv file and retuns the list  which of dictionaries (one dict for record)
		'''
		try:
			reader = csv.DictReader(open(self.csvFile), delimiter = self.delimiter)
		except:
			print('Error Opening the file to read:',self.csvFile)
		result = []
		for row in reader:
			rowdictobj = {}
			for column,value in row.items():
				rowdictobj[column] = value
			result.append(rowdictobj)
		return result

	def count_summary(self):
		print('Total Records: not asked but showing: ', len(self.result))   # Counting Total Records not schools, there may be no schools in some records
		print('Total Schools: ',len([school for school in self.result if school['SCHNAM05'] != '']))
		print('Total unique cities with atleast one school: ',len(set([school['LCITY05'] for school in self.result if school['SCHNAM05'] != ''])))

	def count_list(self,colnam = 'LSTATE05'):
		'''
		This method takes the list with dictionary elements.
		Based on the passed key value of the dictionary it will count 
		the schools per value. Example by state or city or Locale
		Excludes the records with null school value
		'''
	    # If we always know that the list is by state, cate, Locale then in one round prepare all three lists
    	# to save execution time and cost of reading and parsing the list
		if colnam    == 'LSTATE05':
			t1 = 'State'
		elif colnam  == 'LCITY05':
			t1 = 'City'
		elif colnam  ==  'MLOCALE':
			t1 = 'Metro-centric locale'
		else:
			t1 = colnam

		resultcol = []
		for col in self.result:
			if len(col['SCHNAM05']) != 0:
				resultcol.append(col[colnam])
		#print('resultcol: ',resultcol)
		resultx = sorted([(state, resultcol.count(state)) for state in resultcol], key=lambda y: resultcol[1])
		resultx = dict(resultx)
		key=max(resultx.keys(), key=lambda k: resultx[k])
		print('Schools by ' + t1 + ': ')
		print(t1 + ' with most schools:',key, '(',resultx[key], ' schools)')
		for key,val in resultx.items():
			print ('{} {}'.format(key, val));
